import React, { useState } from 'react';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import { Button, Dialog, Toolbar, AppBar, IconButton, DialogContent, Input } from '@material-ui/core';
import { Close } from '@material-ui/icons';

const styles = makeStyles(theme =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        paper: {
            padding: theme.spacing(2),
            textAlign: 'center',
            color: theme.palette.text.secondary,
            width: '400px',
        },
        appBar: {
            position: 'relative',
        },
        title: {
            marginLeft: theme.spacing(2),
            flex: 1,
        },
        textField: {
            marginLeft: theme.spacing(1),
            marginRight: theme.spacing(1),
            width: 200,
        },
    }),
);

export default function CreateAppModal(props) {
    const style = styles();
    const { opened, toggleModal, submitModal } = props;
    const [name, setName] = useState('');
    function handleChange(e) {
        setName(e.target.value);
    }
    return (
        <Dialog onClose={() => toggleModal(false)} aria-labelledby="customized-dialog-title" open={opened}>
            <AppBar className={style.appBar}>
                <Toolbar>
                    <IconButton edge="start" color="inherit" onClick={() => toggleModal(false)} aria-label="close">
                        <Close />
                    </IconButton>
                </Toolbar>
            </AppBar>
            <DialogContent>
                <form onSubmit={e => submitModal(e, name)}>
                    <Input
                        id="standard-name"
                        label="New App"
                        className={style.textField}
                        placeholder="Your App Name"
                        onChange={handleChange}
                    ></Input>
                    <Button
                        disabled={name.length === 0}
                        variant="contained"
                        className="Button"
                        type="submit"
                        value="Submit"
                    >
                        Submit
                    </Button>
                </form>
            </DialogContent>
        </Dialog>
    );
}
