import React, { useState } from 'react';
import { Button, Typography, Card, CardActionArea, CardContent, CardActions, Link } from '@material-ui/core';
export default function AppVersionComponent(props) {
    // const style = styles();
    const { appId, version, uploadVersion, downloadFile } = props;
    const [file, setFile] = useState('');
    function onChange(e) {
        setFile(e.target.files[0]);
    }
    async function onFormSubmit(e) {
        e.preventDefault();
        uploadVersion(appId, version.id, file);
    }
    return (
        <Card>
            <CardActionArea
                disabled={version.file ? false : true}
                onClick={() => {
                    downloadFile(version.file);
                }}
            >
                <CardContent>
                    <Link>
                        <Typography color="primary" gutterBottom variant="h5" component="h2">
                            {version.id} {version.file ? 'Download' : ''}
                        </Typography>
                    </Link>
                </CardContent>
            </CardActionArea>
            <CardActions>
                <form onSubmit={onFormSubmit}>
                    <input type="file" onChange={onChange} />
                    <Button type="submit" size="small" color="primary">
                        Upload
                    </Button>
                </form>
            </CardActions>
        </Card>
    );
}
