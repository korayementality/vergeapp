import React from 'react';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import {
    Button,
    Dialog,
    Typography,
    Slide,
    Toolbar,
    AppBar,
    IconButton,
    DialogContent,
    Grid,
    Input,
} from '@material-ui/core';
import VersionCard from './AppVersionComponent';
import { Close } from '@material-ui/icons';

const styles = makeStyles(theme =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        appBar: {
            position: 'relative',
            color: theme.palette.primary,
        },
        title: {
            marginLeft: theme.spacing(2),
            flex: 1,
        },
        textField: {
            marginLeft: theme.spacing(1),
            marginRight: theme.spacing(1),
            width: 200,
        },
        newVersionFrom: {
            paddingTop: '20px',
        },
    }),
);
// eslint-disable-next-line react/display-name
const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});
export default function AppDetailsComponent(props) {
    const style = styles();
    const {
        app,
        opened,
        toggleModal,
        submitModal,
        handleChange,
        deleteApp,
        uploadVersion,
        downloadFile,
        validId,
    } = props;
    const versionsList = Object.values(app.versions).map((version, index) => {
        return (
            <Grid key={index} item xs={4}>
                <VersionCard
                    appId={app.id}
                    uploadVersion={uploadVersion}
                    version={version}
                    downloadFile={downloadFile}
                ></VersionCard>
            </Grid>
        );
    });
    return (
        <Dialog
            fullScreen
            onClose={() => toggleModal(false)}
            aria-labelledby="customized-dialog-title"
            open={opened}
            TransitionComponent={Transition}
        >
            <AppBar className={style.appBar}>
                <Toolbar>
                    <IconButton edge="start" color="inherit" onClick={() => toggleModal(false)} aria-label="close">
                        <Close />
                    </IconButton>
                    <Typography color="initial" variant="h4" className={style.title}>
                        {app && app.name}
                    </Typography>
                    <Button
                        color="secondary"
                        variant="contained"
                        onClick={() => {
                            toggleModal(false);
                            deleteApp(app.id);
                        }}
                    >
                        Delete App
                    </Button>
                </Toolbar>
            </AppBar>
            <DialogContent>
                <Grid container spacing={2} direction={'row'} justify={'flex-start'} alignItems={'center'}>
                    {versionsList}
                </Grid>
                <form className={style.newVersionFrom} onSubmit={submitModal}>
                    <Input
                        id="standard-name"
                        label="New Version"
                        className={style.textField}
                        placeholder="Ex: 1.0.1-alpha.23"
                        onChange={handleChange}
                    ></Input>
                    <Button
                        disabled={!validId}
                        color="primary"
                        variant="contained"
                        className="Button"
                        type="submit"
                        value="Submit"
                    >
                        Submit
                    </Button>
                </form>
            </DialogContent>
        </Dialog>
    );
}
