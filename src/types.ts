export interface VergeApp {
    id: string;
    name: string;
    versions: { id: { id: string; file: string | null } };
}
