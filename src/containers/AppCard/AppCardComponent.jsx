import React, { useState } from 'react';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import { Grid, Button, Typography, Card } from '@material-ui/core';
import AppDetailsComponent from '../../components/AppDetailsComponent';

const styles = makeStyles(theme =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        paper: {
            padding: theme.spacing(2),
            textAlign: 'center',
            width: '400px',
        },
        appBar: {
            position: 'relative',
        },
        title: {
            marginLeft: theme.spacing(2),
            flex: 1,
        },
        textField: {
            marginLeft: theme.spacing(1),
            marginRight: theme.spacing(1),
            width: 200,
        },
    }),
);

export default function AppCardComponent(props) {
    const style = styles();
    let { app, deleteApp, addVersion, uploadVersion, downloadFile } = props;
    let toggleModal;
    let opened;
    [opened, toggleModal] = useState(false);
    const [newVersion, updateVersion] = useState('');
    const [validId, setValidity] = useState(false);
    const validateId = id => {
        const result = id.match(
            /^([0-9]+)\.([0-9]+)\.([0-9]+)(?:-([0-9A-Za-z-]+(?:\.[0-9A-Za-z-]+)*))?(?:\+[0-9A-Za-z-]+)?$/,
        );
        if (result !== null) setValidity(true);
        else setValidity(false);
    };
    const handleSubmit = e => {
        e.preventDefault();
        addVersion(app.id, newVersion);
    };
    const handleChange = e => {
        validateId(e.target.value);
        updateVersion(e.target.value);
    };
    return (
        <Grid item xs={4}>
            <Card className={style.paper}>
                <Typography variant="h4" gutterBottom color="primary">
                    {app && app.name}
                </Typography>
                <Button color="primary" variant="contained" size="small" onClick={() => toggleModal(true)}>
                    <Typography variant="button" display="block" gutterBottom>
                        View App
                    </Typography>
                </Button>
            </Card>
            <AppDetailsComponent
                app={app}
                submitModal={handleSubmit}
                toggleModal={toggleModal}
                uploadVersion={uploadVersion}
                opened={opened}
                handleChange={handleChange}
                newVersion={newVersion}
                deleteApp={deleteApp}
                downloadFile={downloadFile}
                validId={validId}
            ></AppDetailsComponent>
        </Grid>
    );
}
