/* eslint-disable @typescript-eslint/no-use-before-define */
import React, { useState } from 'react';
import AppCardComponent from './AppCardComponent';
import * as actions from './AppCardActions';
export default function AppCardContainer(props) {
    let { app, deleteApp } = props;
    let setApp;
    [app, setApp] = useState(app);
    async function addVersion(id, version) {
        setApp(await actions.addVersion(id, version));
    }
    async function uploadVersion(id, version, file) {
        setApp(await actions.uploadVersion(id, version, file));
    }
    async function downloadFile(version) {
        await actions.downloadFile(version);
    }

    return (
        <AppCardComponent
            app={app}
            addVersion={addVersion}
            deleteApp={deleteApp}
            uploadVersion={uploadVersion}
            downloadFile={downloadFile}
        />
    );
}
