import axios from 'axios';
export const getApp = async appId => {
    const response = await axios.get('/api/apps/' + appId);
    const stringifiedVersions = JSON.stringify(response.data.versions);
    const versionsParsed = JSON.parse(stringifiedVersions);
    response.data.versions = versionsParsed;
    const { id, name, versions } = await response.data;
    return {
        id,
        name,
        versions,
    };
};
export const addVersion = async (id, version) => {
    await axios.post('/api/apps/' + id + '/' + version);
    return getApp(id);
};

export const uploadVersion = async (id, version, file) => {
    const url = '/api/apps/' + id + '/' + version + '/file';
    const formData = new FormData();
    formData.append('file', file);
    const config = {
        headers: {
            'content-type': 'multipart/form-data',
        },
    };
    await axios.post(url, formData, config);
    return getApp(id);
};

export const downloadFile = async filePath => {
    const response = await axios.get(filePath, { responseType: 'blob' });
    const url = window.URL.createObjectURL(new Blob([response.data]));
    const link = document.createElement('a');
    link.href = url;
    const fileName = filePath.replace('/api/files/', '');
    link.setAttribute('download', fileName);
    document.body.appendChild(link);
    link.click();
};
