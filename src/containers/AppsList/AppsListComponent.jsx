import React, { useState } from 'react';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import { Grid, Button } from '@material-ui/core';
import AppCard from '../AppCard/AppCardContainer';
import CreateApp from '../../components/CreateAppModal';

const useStyles = makeStyles(theme =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        paper: {
            padding: theme.spacing(2),
            textAlign: 'center',
            color: theme.palette.text.primary,
            width: '400px',
        },
        appBar: {
            position: 'relative',
        },
        title: {
            marginLeft: theme.spacing(2),
            flex: 1,
        },
        textField: {
            marginLeft: theme.spacing(1),
            marginRight: theme.spacing(1),
            width: 200,
        },
        createAppButton: {
            fontWeight: 'bold',
            position: 'fixed',
            bottom: '20px',
            right: '15px',
            borderRadius: '40px;',
        },
    }),
);

export default function AppsList(props) {
    const styles = useStyles();
    const [opened, setOpened] = useState(false);
    const { apps, deleteApp, createApp } = props;
    function toggleModal() {
        setOpened(!opened);
    }
    function submitModal(e, name) {
        e.preventDefault();
        createApp(name);
        toggleModal();
    }
    return (
        <div className={styles.root}>
            <Grid container direction={'row'} justify={'flex-start'} alignItems={'flex-start'} spacing={2}>
                {apps.map((app, key) => {
                    return <AppCard key={key} app={app} deleteApp={deleteApp}></AppCard>;
                })}
                <Button variant="contained" color="secondary" className={styles.createAppButton} onClick={toggleModal}>
                    Create App
                </Button>
                <CreateApp opened={opened} submitModal={submitModal} toggleModal={toggleModal}></CreateApp>
            </Grid>
        </div>
    );
} /* <AppDetailComponent opened={false} key={key} app={app}></AppDetailComponent> */
