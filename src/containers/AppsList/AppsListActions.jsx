import axios from 'axios';

export const getApps = async () => {
    const response = await axios.get('/api/apps');
    const apps = response.data.map(app => {
        let string = JSON.stringify(app);
        return JSON.parse(string);
    });
    return apps;
};
export const deleteApp = async id => {
    await axios.delete('/api/apps/' + id);
    return getApps();
};

export const createNewApp = async name => {
    const id = name.trim();
    await axios.post('/api/apps/' + id, {
        name,
    });
    return getApps();
};
