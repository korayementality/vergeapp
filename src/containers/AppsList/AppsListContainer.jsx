import React, { useEffect, useState } from 'react';
import AppsListComponent from './AppsListComponent';
import * as actions from './AppsListActions';
export default function AppsList() {
    let apps;
    let setApps;
    [apps, setApps] = useState([]);
    const createApp = async name => {
        setApps(await actions.createNewApp(name));
    };
    const deleteApp = async id => {
        setApps(await actions.deleteApp(id));
    };
    const getApps = async () => {
        setApps(await actions.getApps());
    };
    useEffect(() => {
        getApps();
    }, []);

    return <AppsListComponent apps={apps} createApp={createApp} deleteApp={deleteApp} />;
}
