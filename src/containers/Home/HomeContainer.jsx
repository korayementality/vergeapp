import React from 'react';
import AppsList from '../AppsList/AppsListContainer';
import { Typography } from '@material-ui/core';
export default function Home() {
    return (
        <div>
            <Typography
                color="secondary"
                variant="h2"
                style={{ textAlign: 'center', padding: '10px', fontWeight: 'bold' }}
            >
                VERGE
            </Typography>
            <AppsList></AppsList>
        </div>
    );
}
