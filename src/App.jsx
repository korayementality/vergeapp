import React, { Component } from 'react';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import MyTheme from './theme';
import Home from './containers/Home/HomeContainer';

class App extends Component {
    render() {
        const muiTheme = createMuiTheme(MyTheme);
        return (
            <MuiThemeProvider theme={muiTheme}>
                <div className="App">
                    <Home></Home>
                </div>
            </MuiThemeProvider>
        );
    }
}
export default App;
