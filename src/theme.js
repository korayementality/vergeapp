import { createMuiTheme } from '@material-ui/core/styles';
import { teal, blue } from '@material-ui/core/colors';

export default createMuiTheme({
    overrides: {
        MuiButton: {},
    },
    palette: {
        primary: { main: teal[400] }, // Purple and green play nicely together.
        secondary: { main: blue[800] }, // This is just green.A700 as hex.
    },
});
